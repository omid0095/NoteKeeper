package com.omid.notekeeper.Presenter;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.notes.NotesPresenter;
import com.omid.notekeeper.ui.notes.NotesView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Omid on 9/1/17.
 */

public class NotesPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void init() {
        // replace Android main thread.
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Test
    public void newNoteTest() throws Exception{
        Folder folder=mock(Folder.class);
        NotesContext context=mock(NotesContext.class);
        INoteStorage noteStorage= PresenterUtils.mockStorage();
        NotesView notesView=PresenterUtils.mockView(NotesView.class);
        NotesPresenter presenter=new NotesPresenter(folder,context,noteStorage,notesView);

        presenter.createNewNote("a name");  //presenter is asked for creating new note
        Thread.sleep(100);
        verify(notesView).openNote();   //presenter should ask the view to open the note
    }

    @Test
    public void newNoteWithBadNameTest() throws InterruptedException {

        Folder folder=mock(Folder.class);
        NotesContext context=mock(NotesContext.class);
        INoteStorage noteStorage=PresenterUtils.mockStorage();
        NotesView notesView=PresenterUtils.mockView(NotesView.class);
        NotesPresenter presenter=new NotesPresenter(folder,context,noteStorage,notesView);


        Observable<Note> error=Observable.error(new Exception("err"));// error observable with "err" msg

        //mock view's showError method
        when(notesView.showError(anyString())).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));

        //storage throws the exception on addNote with bad name
        when(noteStorage.addNote(any(Folder.class),eq("badName")))
                .thenReturn(error);

        //presenter is asked for creating new note with bad name
        presenter.createNewNote("badName");

        Thread.sleep(100);

        //presenter showuld ask view to show error with "err" message
        verify(notesView).showError("err");
    }


}
