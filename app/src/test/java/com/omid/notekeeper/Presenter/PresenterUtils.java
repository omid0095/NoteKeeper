package com.omid.notekeeper.Presenter;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.IBaseView;

import org.mockito.Mockito;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * This utility creates mockView and Storage for testing presenters
 * Created by Omid on 9/2/17.
 */

public class PresenterUtils {


    public static <T extends IBaseView> T mockView(Class<T> clazz) {

        return mockView(clazz, "");
    }

    /**
     * create a mock view extends of BaseView class with implemented observable methods
     *
     * @param clazz         the class of view which should be mocked
     * @param askTextResult the text that askForText method should return
     * @return the mock class
     */
    public static <T extends IBaseView> T mockView(Class<T> clazz, String askTextResult) {
        T mockView = mock(clazz, Mockito.CALLS_REAL_METHODS);

        when(mockView.showMessage(anyString(), anyString())).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));
        when(mockView.askAQuestion(anyString(), anyString())).thenReturn(Observable.just(true));
        when(mockView.askForText(anyString(), anyString(), anyString())).thenReturn(Observable.just(askTextResult));
        return mockView;
    }


    /**
     * provide a mock NoteStorage whith implemented Observable methods
     * @return mock NoteStorage
     */
    public static INoteStorage mockStorage() {
        INoteStorage noteStorage = mock(INoteStorage.class);
        when(noteStorage.deleteFolder(any(Folder.class))).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));
        when((noteStorage.addFolder(anyString()))).thenReturn(Observable.just(mock(Folder.class)));
        when(noteStorage.addNote(any(Folder.class), anyString())).thenReturn(Observable.just(mock(Note.class)));
        when(noteStorage.deleteNote(any(Note.class))).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));
        when(noteStorage.saveNote(any(Note.class))).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));
        return noteStorage;
    }
}
