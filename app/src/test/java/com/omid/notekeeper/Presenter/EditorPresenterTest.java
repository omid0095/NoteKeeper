package com.omid.notekeeper.Presenter;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.editor.EditorPresenter;
import com.omid.notekeeper.ui.editor.EditorView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit Test for EditorPresenter
 *
 * Created by Omid on 9/1/17.
 */

public class EditorPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void init() {
        // replace Android main thread.
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Test
    public void closeAfterDeleteTest() throws InterruptedException {

        //create mock objects
        Note note = mock(Note.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        EditorView editorView = PresenterUtils.mockView(EditorView.class);
        EditorPresenter presenter = new EditorPresenter(noteStorage, note, editorView);

        /*
            it is expected when deleteConfirmed is called,
            then presenter ask view to close.
         */
        presenter.onDeleteConfirmed();
        Thread.sleep(100);
        verify(editorView).close();
    }

    @Test
    public void showSaveBtnOnTextChangeTest() {
        Note note = mock(Note.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        EditorView editorView = PresenterUtils.mockView(EditorView.class);
        EditorPresenter presenter = new EditorPresenter(noteStorage, note, editorView);

        /*
            it is expected when text changes, presenter ask view to show
            the save button
         */
        presenter.onRichTextChanged();
        verify(editorView).showSave(true);
    }

    @Test
    public void onBackClickInSaveModes() {
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        Note note = mock(Note.class);
        EditorView editorView = PresenterUtils.mockView(EditorView.class);
        EditorPresenter presenter = new EditorPresenter(noteStorage, note, editorView);

        presenter.onDrawerChanged();    //something is changed
        presenter.onBackClick();    //user wants to leave
        verify(editorView).leaveWithoutSave();  //nobody saved the changes, tell view to leave without save

    }

    @Test
    public void onBackClickInUnSaveModes() {
        Note note = mock(Note.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        EditorView editorView = PresenterUtils.mockView(EditorView.class);
        EditorPresenter presenter = new EditorPresenter(noteStorage, note, editorView);

        presenter.onSaveClick();    //user click on save
        presenter.onBackClick();    //user wants to leave
        verify(editorView).close(); //presenter ask with to be closed

    }
}
