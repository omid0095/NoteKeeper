package com.omid.notekeeper.Presenter;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.ui.folder.FolderPresenter;
import com.omid.notekeeper.ui.folder.FolderView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit Test for FolderPresenter
 * Created by Omid on 9/1/17.
 */

public class FolderPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void init() {
        // replace Android main thread.
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Test
    public void newFolderTest() throws Exception {
        NotesContext context = mock(NotesContext.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        FolderView folderView = PresenterUtils.mockView(FolderView.class);
        FolderPresenter presenter = new FolderPresenter(noteStorage, context, folderView);


        presenter.createNewFolder("new folder");    //presenter is asked for creating a folder
        Thread.sleep(100);
        verify(folderView).openFolder();    //presenter should ask view to open the folder
    }

    @Test
    public void newFolderWithBadNameTest() throws InterruptedException {

        NotesContext context = mock(NotesContext.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();

        //view returns a bad name when it is asked for a name
        FolderView folderView = PresenterUtils.mockView(FolderView.class,"badName");
        FolderPresenter presenter = new FolderPresenter(noteStorage, context, folderView);

        // message of error is "err"
        Observable<Folder> error = Observable.error(new Exception("err"));

        //Storage throws exception when user wants to create folder with bad name
        when(noteStorage.addFolder(eq("badName"))).thenReturn(error);

        //mock showError method on view
        when(folderView.showError(anyString())).thenReturn(Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                e.onComplete();
            }
        }));

        presenter.createNewFolder("badName");   //presenter is asked to create badName folder
        Thread.sleep(100);
        verify(folderView).showError("err");    //presenter should ask view to show the "err" error
    }

    @Test
    public void deleteFolderTest() throws InterruptedException {
        NotesContext context = mock(NotesContext.class);
        INoteStorage noteStorage = PresenterUtils.mockStorage();
        FolderView folderView = PresenterUtils.mockView(FolderView.class);
        FolderPresenter presenter = new FolderPresenter(noteStorage, context, folderView);

        //view returns true when it is asked for delete
        when(folderView.askForDelete()).thenReturn(Observable.just(true));

        //fire presenter's listener: folderdelete is clicked
        presenter.getFolderClickListener().onFolderDeleteClick(mock(Folder.class));
        Thread.sleep(100);

        //it is expected that presenter notify view for deleting
        verify(folderView).notifyFolderRemove(any(Folder.class));
    }

}
