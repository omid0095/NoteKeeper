package com.omid.notekeeper.model;

import com.j256.ormlite.dao.Dao;
import com.omid.notekeeper.database.FolderEntity;
import com.omid.notekeeper.database.NoteEntity;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * MockDao represents Dao for tests
 * Created by Omid on 9/1/17.
 */

public class MockDao {

    /**
     * @return a mockDao for FolderEntity, with simple implementation
     */
    public static Dao<FolderEntity, Integer> getMockFolderDao() {
        try {
            //define a list to store folders
            final List<FolderEntity> folders = new ArrayList<>();

            //create a mockDao
            Dao<FolderEntity, Integer> mockDao = mock(Dao.class);

            //create a FolderEntity and put it to the list, when create method is called
            when(mockDao.create(any(FolderEntity.class))).thenAnswer(new Answer<Integer>() {
                @Override
                public Integer answer(InvocationOnMock invocation) throws Throwable {
                    folders.add((FolderEntity) invocation.getArgument(0));
                    return 0;
                }
            });

            //return list when Dao is asked for all folderEntities
            when(mockDao.queryForAll()).thenReturn(folders);

            //remove from list when Dao is asked for a folder
            when(mockDao.delete(any(FolderEntity.class))).then(new Answer<Integer>() {
                @Override
                public Integer answer(InvocationOnMock invocation) throws Throwable {
                    folders.remove((FolderEntity) invocation.getArgument(0));
                    return 0;
                }
            });
            return mockDao;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * @return a mockDao for NoteEntity, with simple implementation
     */
    public static Dao<NoteEntity, Integer> getMockNoteDao() {
        try {
            //define a list to store notes
            final List<NoteEntity> notes = new ArrayList<>();

            //create a mockDao
            Dao<NoteEntity, Integer> mockDao = mock(Dao.class);

            //create a NoteEntity and put it to the list, when create method is called
            when(mockDao.create(any(NoteEntity.class))).thenAnswer(new Answer<Integer>() {
                @Override
                public Integer answer(InvocationOnMock invocation) throws Throwable {
                    notes.add((NoteEntity) invocation.getArgument(0));
                    return 0;
                }
            });

            //return list when Dao is asked for all NoteEntities
            when(mockDao.queryForAll()).thenReturn(notes);

            //remove from list when Dao is asked for a folder
            when(mockDao.delete(any(NoteEntity.class))).then(new Answer<Integer>() {
                @Override
                public Integer answer(InvocationOnMock invocation) throws Throwable {
                    notes.remove((NoteEntity) invocation.getArgument(0));
                    return 0;
                }
            });
            return mockDao;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
