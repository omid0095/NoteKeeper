package com.omid.notekeeper.model;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.InputValidator;
import com.omid.notekeeper.database.FolderEntity;
import com.omid.notekeeper.database.NoteStorageDBImp;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit Tests for testing NoteStorageImp
 * Created by Omid on 9/1/17.
 */
public class NotesStorageTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testAddFolder() {

        final Dao folderDao = MockDao.getMockFolderDao();
        Dao notesDao = mock(Dao.class);
        InputValidator inputValidator = mock(InputValidator.class);

        //create noteStorage imp using mock Objects
        INoteStorage noteStorage = new NoteStorageDBImp(folderDao, notesDao, inputValidator);

        noteStorage.addFolder("f1").blockingFirst();        //add a folder to the storage
        List<Folder> folders = noteStorage.getFolders().blockingFirst();
        Assert.assertEquals(1, folders.size());     //expect one object in storage

        noteStorage.addFolder("f2").blockingFirst();    //add another folder
        folders = noteStorage.getFolders().blockingFirst();
        Assert.assertEquals(2, folders.size());     //expect two folders in the storage

    }

    @Test
    public void testAddAndRemoveFolder() {
        Dao folderDao = MockDao.getMockFolderDao();
        Dao notesDao = mock(Dao.class);
        InputValidator inputValidator = mock(InputValidator.class);
        INoteStorage noteStorage = new NoteStorageDBImp(folderDao, notesDao, inputValidator);

        noteStorage.addFolder("f1").blockingFirst();        //add a folder

        List<Folder> folders = noteStorage.getFolders().blockingFirst();
        Assert.assertEquals(1, folders.size());

        noteStorage.addFolder("f2").blockingFirst();    //add another folder
        noteStorage.deleteFolder(folders.get(0)).blockingSubscribe();   //remove first folder

        folders = noteStorage.getFolders().blockingFirst();
        Assert.assertEquals(1, folders.size());     //so we have one folder
    }

    @Test(expected = Exception.class)
    public void inputValidatorTest() throws Exception {

        /*
            when inputValidator throws exception for bad folder name.
            it is expected note storage faces an exception on adding folder
            with bad name.
         */
        Dao folderDao = mock(Dao.class);
        Dao notesDao = mock(Dao.class);
        InputValidator inputValidator = mock(InputValidator.class);
        INoteStorage noteStorage = new NoteStorageDBImp(folderDao, notesDao, inputValidator);

        doThrow(new Exception()).when(inputValidator).validateFolderName("bad name");

        noteStorage.addFolder("bad name").blockingFirst();
    }

    @Test
    public void testAddNote() throws SQLException {
        final Dao notesDao = MockDao.getMockNoteDao();
        FolderEntity folder = mock(FolderEntity.class);
        Dao folderDao = mock(Dao.class);
        InputValidator inputValidator = mock(InputValidator.class);
        INoteStorage noteStorage = new NoteStorageDBImp(folderDao, notesDao, inputValidator);

        //return all Notes in the NotesDao, when folder is asked for getNotes
        when(folder.getNotes()).thenReturn(mock(ForeignCollection.class));
        when(folder.getNotes().toArray(any(Note[].class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return notesDao.queryForAll().toArray(new Note[0]);
            }
        });

        // return the mockFolder when folderDao is asked for folder with any id
        when(folderDao.queryForId(anyInt())).thenReturn(folder);

        noteStorage.addNote(folder, "f1").blockingFirst();   //add a note to the folder
        List<Note> notes = noteStorage.getNotes(folder).blockingFirst();
        Assert.assertEquals(1, notes.size());   //expect one note in the folder
        noteStorage.addNote(folder, "f2").blockingFirst();   //add another note to the folder
        notes = noteStorage.getNotes(folder).blockingFirst();
        Assert.assertEquals(2, notes.size());   //expected two notes in the folder

    }


}
