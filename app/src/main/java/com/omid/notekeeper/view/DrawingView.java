package com.omid.notekeeper.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import com.omid.notekeeper.R;

/**
 * A view with posibility of drawing
 * Created by Omid on 8/31/17.
 */

public class DrawingView extends View {

    private int lineColor;
    private float lineWidth;
    private int cursorColor;

    public void setOnDrawListener(DrawingView.onDrawListener onDrawListener) {
        this.onDrawListener = onDrawListener;
    }

    public interface onDrawListener {
        void onDraw(DrawingView drawingView);
    }

    public int width;
    public int height;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint;
    Context context;
    private Paint cursorPaint;
    private Path circlePath;
    private Paint penPaint;
    private boolean editing;

    private onDrawListener onDrawListener;

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap bitmap) {
        mBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        mCanvas = new Canvas(mBitmap);
    }


    public DrawingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        getAttributes(context, attrs);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        setupDefaultCursor();
        setupDefaultPen();
    }

    /**
     * extract user attributes from the attrs.
     * @param context
     * @param attrs
     */
    private void getAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DrawingView,
                0, 0);
        try {
            lineColor = a.getColor(R.styleable.DrawingView_line_color, Color.BLACK);
            cursorColor = a.getColor(R.styleable.DrawingView_cursor_color, Color.BLACK);
            lineWidth = a.getDimension(R.styleable.DrawingView_line_width, 1);
        } finally {
            a.recycle();
        }
    }

    public Paint getPenPaint() {
        return penPaint;
    }

    private void setupDefaultPen() {
        penPaint = new Paint();
        penPaint.setAntiAlias(true);
        penPaint.setDither(true);
        penPaint.setColor(lineColor);
        penPaint.setStyle(Paint.Style.STROKE);
        penPaint.setStrokeJoin(Paint.Join.ROUND);
        penPaint.setStrokeCap(Paint.Cap.ROUND);
        penPaint.setStrokeWidth(lineWidth);
    }

    private void setupDefaultCursor() {
        cursorPaint = new Paint();
        circlePath = new Path();
        cursorPaint.setAntiAlias(true);
        cursorPaint.setColor(cursorColor);
        cursorPaint.setStyle(Paint.Style.STROKE);
        cursorPaint.setStrokeJoin(Paint.Join.MITER);
        cursorPaint.setStrokeWidth(4f);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mBitmap == null)
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        if (mCanvas == null)
            mCanvas = new Canvas(mBitmap);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath(mPath, penPaint);
        canvas.drawPath(circlePath, cursorPaint);
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;

            circlePath.reset();
            circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        circlePath.reset();
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, penPaint);
        // kill this so we don't double draw
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!editing)
            return false;
        fireDrawListener();
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }

    private void fireDrawListener() {
        if (onDrawListener != null)
            onDrawListener.onDraw(this);
    }

    public void setEditing(boolean editing) {
        this.editing = editing;
    }

    public void setOnDrawListener() {
        this.onDrawListener = onDrawListener;
    }
}
