package com.omid.notekeeper.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import com.omid.notekeeper.R;

import io.reactivex.functions.Consumer;


/**
 * Created by Omid on 8/30/17.
 */

public class InputDialogBuilder extends AlertDialog.Builder {

    public interface InputDialogListener {

        void onOKPressed(String text);
    }

    private final InputDialogListener listener;
    private EditText editText;

    public InputDialogBuilder(Context context, InputDialogListener listener) {
        super(context,R.style.dialogTheme);
        this.listener = listener;
        initPossitiveBtn();
        initNegativeBtn();
        initEditText();
    }

    private void initEditText() {
        editText = new EditText(getContext());
        setView(editText);
    }

    private void initNegativeBtn() {
        setNegativeButton(R.string.input_dlg_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
    }

    private void initPossitiveBtn() {
        setPositiveButton(R.string.input_dlg_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onOKPressed(editText.getText().toString());
                dialog.cancel();
            }
        });
    }

    public InputDialogBuilder setHint(String hint) {
        editText.setHint(hint);
        return this;
    }
}
