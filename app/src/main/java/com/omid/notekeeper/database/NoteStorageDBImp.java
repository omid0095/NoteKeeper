package com.omid.notekeeper.database;

import android.graphics.Bitmap;

import com.j256.ormlite.dao.Dao;
import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.InputValidator;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Implementation of Note Storage using database.
 * Created by Omid on 8/30/17.
 */

public class NoteStorageDBImp implements INoteStorage {

    /*
    Since all of the Note/Folder objects which are using here
        are made by this implementation so we can make sure that
        they are instance of NoteEntity/FolderEntity and cast them. In case of
        other implementation has made Note/Folder objects, This class
        is not responsible for them and throws exception.
     */

    private final Dao<FolderEntity, Integer> folderDao;
    private final Dao<NoteEntity, Integer> noteDao;
    private final InputValidator validator;

    public NoteStorageDBImp(Dao<FolderEntity, Integer> folderDao, Dao<NoteEntity, Integer> noteDao, InputValidator validator) {
        this.folderDao = folderDao;
        this.noteDao = noteDao;
        this.validator = validator;
    }

    @Override
    public Observable<List<Folder>> getFolders() {
        return Observable.fromCallable(new Callable<List<Folder>>() {
            @Override
            public List<Folder> call() throws Exception {
                return Arrays.asList(folderDao.queryForAll().toArray(new Folder[0]));
            }
        });
    }

    @Override
    public Observable<List<Note>> getNotes(final Folder folder) {
        final FolderEntity folderEntity = (FolderEntity) folder;
        return Observable.fromCallable(new Callable<List<Note>>() {
            @Override
            public List<Note> call() throws Exception {
                Note[] notes = folderDao.queryForId(folderEntity.getId()).getNotes().toArray(new Note[0]);
                return Arrays.asList(notes);
            }
        });
    }

    @Override
    public Observable<Folder> addFolder(final String name) {
        return Observable.fromCallable(new Callable<Folder>() {
            @Override
            public Folder call() throws Exception {
                //first validate the name
                validator.validateFolderName(name);
                FolderEntity folder = new FolderEntity(name);

                //add the folder to the Dao
                folderDao.create(folder);

                //and finally return it
                return folder;
            }
        });
    }

    @Override
    public Observable<Note> addNote(final Folder folder, final String name) {
        return Observable.fromCallable(new Callable<Note>() {
            @Override
            public Note call() throws Exception {
                //first validate the name
                validator.validateNoteName(name);
                NoteEntity note = new NoteEntity((FolderEntity) folder, name);

                //add the note to the database
                noteDao.create(note);

                // and finally return it.
                return note;
            }
        });
    }

    @Override
    public Observable<Void> deleteFolder(final Folder folder) {
        return Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                folderDao.delete((FolderEntity) folder);
                e.onComplete();

            }
        });
    }

    @Override
    public Observable<Void> deleteNote(final Note note) {
        return Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                noteDao.delete((NoteEntity) note);
                e.onComplete();

            }
        });
    }

    @Override
    public Observable<Void> saveNote(final Note note) {
        final NoteEntity noteE = (NoteEntity) note;
        return Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Void> e) throws Exception {
                noteDao.update(noteE);
                e.onComplete();
            }
        });
    }

}
