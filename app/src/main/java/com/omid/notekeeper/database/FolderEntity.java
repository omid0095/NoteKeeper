package com.omid.notekeeper.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.omid.notekeeper.model.Folder;

import static com.omid.notekeeper.database.FolderEntity.FIELD_NAME_ID;
import static com.omid.notekeeper.database.FolderEntity.FIELD_NAME_NAME;
import static com.omid.notekeeper.database.FolderEntity.FIELD_NAME_NOTES;
import static com.omid.notekeeper.database.FolderEntity.TABLE_NAME_FOLDER;

/**
 * An implementation of Folder, which can be used by database.
 * The database annotation describes the folders table in the database
 * Created by Omid on 8/30/17.
 */
@DatabaseTable(tableName = TABLE_NAME_FOLDER)
public class FolderEntity implements Folder {
    public static final String TABLE_NAME_FOLDER = "folders";

    public static final String FIELD_NAME_ID = "id";

    public static final String FIELD_NAME_NAME = "name";

    public static final String FIELD_NAME_NOTES = "notes";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_NAME_NAME)
    private String name;

    //in design of database 1 folder has a relation with * notes
    @ForeignCollectionField(columnName = FIELD_NAME_NOTES, eager = false)
    private ForeignCollection<NoteEntity> notes;


    /**
     * no-arg constructor for database
     */
    public FolderEntity() {

    }

    public FolderEntity(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getContentsCount() {
        // if there is no notes object so there is no note related with this object
        return notes != null ? notes.size() : 0;
    }

    public ForeignCollection<NoteEntity> getNotes() {
        return notes;
    }
}
