package com.omid.notekeeper.database;

import android.content.Context;

import com.omid.notekeeper.R;
import com.omid.notekeeper.api.InputValidator;

import java.sql.SQLException;
import java.util.List;

/**
 * An implementation of InputValidator based on Database.
 * Created by Omid on 8/30/17.
 */

public class InputValidatorDBImp implements InputValidator {

    private final DatabaseHelper databaseHelper;
    private final Context context;

    public InputValidatorDBImp(Context context, DatabaseHelper databaseHelper) {
        this.context = context;
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void validateFolderName(String folderName) throws Exception {
        //new folder should have a name
        if (folderName == null || folderName.equals(""))
            throw new Exception(context.getString(R.string.input_err_folder_noname));

        //a duplicate name is not acceptable
        List<FolderEntity> list = databaseHelper.getFolderDao().queryForAll();
        for (FolderEntity folder : list) {
            if (folder.getName().equals(folderName))
                throw new Exception(context.getString(R.string.input_err_folder_exists));
        }
    }

    @Override
    public void validateNoteName(String name) throws Exception {
        //note name shouldn't be null or empty
        if (name == null || name.equals(""))
            throw new Exception(context.getString(R.string.input_err_note_noname));

        //throw exception if there is a note with this name
        List<NoteEntity> list = databaseHelper.getNoteDao().queryForAll();
        for (NoteEntity note : list) {
            if (note.getName().equals(name))
                throw new Exception(context.getString(R.string.input_err_note_exists));
        }
    }
}
