package com.omid.notekeeper.database;

import android.graphics.Bitmap;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.util.BitmapUtil;

import static com.omid.notekeeper.database.NoteEntity.TABLE_NAME_NOTE;


/**
 * An implementation of Note, which can be used by database.
 * The database annotation describes the notes table in the database
 * Created by Omid on 8/30/17.
 */
@DatabaseTable(tableName = TABLE_NAME_NOTE)
public class NoteEntity implements Note {
    public static final String TABLE_NAME_NOTE = "notes";

    public static final String FIELD_NAME_ID = "id";

    public static final String FIELD_NAME_NAME = "name";

    public static final String FIELD_NAME_STRING = "str";

    public static final String FIELD_NAME_BITMAP = "bitmap";

    public static final String FIELD_NAME_FOLDER = "folder";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_NAME_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_NAME_STRING)
    private String string;

    //ORMLite can't store a bitmap, so we translate it to a string.
    @DatabaseField(columnName = FIELD_NAME_BITMAP)
    String bitmap;

    //in design of database each note is related to a folder
    @DatabaseField(columnName = FIELD_NAME_FOLDER, foreign = true, foreignAutoRefresh = true)
    FolderEntity folder;

    /**
     * no-arg constructor for database
     */
    public NoteEntity() {

    }

    public NoteEntity(FolderEntity folder, String name) {
        this.folder = folder;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public void setString(String string) {
        this.string = string;
    }

    @Override
    public Bitmap getBitmap() {

        //translate bitmap field (which is String) to a bitmap using utility
        if (bitmap != null)
            return BitmapUtil.getBitmap(bitmap);
        else
            return null;
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
//        translate the Bitmap Object to a string and then store it in bitmap field
        this.bitmap = BitmapUtil.getString(bitmap);
    }
}
