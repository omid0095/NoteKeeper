package com.omid.notekeeper.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * A helper in working with database. this class keeps database file,
 * Dao for Folders and Notes.
 * Created by Omid on 8/30/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "notekeeper.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<FolderEntity, Integer> folderDao;
    private Dao<NoteEntity, Integer> noteDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            //on create of database, we have to create tables for Folder entities and Note entities.
            TableUtils.createTableIfNotExists(connectionSource, FolderEntity.class);
            TableUtils.createTableIfNotExists(connectionSource, NoteEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            // TODO: 9/3/17 don't drop a table just update it based on version
            TableUtils.dropTable(connectionSource, FolderEntity.class, true);
            TableUtils.dropTable(connectionSource, NoteEntity.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return the FolderEntity dao
     */
    public Dao<FolderEntity, Integer> getFolderDao() {
        if (folderDao == null)
            try {
                folderDao = getDao(FolderEntity.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return folderDao;
    }

    /**
     *
     * @return The note entity dao
     */
    public Dao<NoteEntity, Integer> getNoteDao() {
        if (noteDao == null)
            try {
                noteDao = getDao(NoteEntity.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return noteDao;
    }
}
