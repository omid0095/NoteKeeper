package com.omid.notekeeper.dagger.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The module of application provides application context
 * Created by Omid on 8/30/17.
 */
@Module
public class AppModule {
    private final Context appContext;

    public AppModule(Context applicationContext) {
        this.appContext = applicationContext;
    }

    @Provides
    @Singleton
    Context getAppContext() {
        return appContext;
    }

}
