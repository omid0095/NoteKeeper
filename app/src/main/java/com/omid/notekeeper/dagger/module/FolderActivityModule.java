package com.omid.notekeeper.dagger.module;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.ui.folder.FolderPresenter;
import com.omid.notekeeper.ui.folder.FolderView;
import com.omid.notekeeper.ui.folder.FoldersActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/30/17.
 */
@Module
public class FolderActivityModule {

    private final FoldersActivity folderActivity;

    public FolderActivityModule(FoldersActivity foldersActivity) {
        this.folderActivity = foldersActivity;
    }

    @Provides
    @PerActivity
    public FolderView provideFolderView() {
        return folderActivity;
    }

    @Provides
    @PerActivity
    public FolderPresenter getFolderPresenter(INoteStorage noteStorage, NotesContext notesContext, FolderView view){
        return new FolderPresenter(noteStorage,notesContext,view);
    }
}
