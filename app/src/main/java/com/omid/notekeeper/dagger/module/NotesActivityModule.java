package com.omid.notekeeper.dagger.module;

import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.ui.folder.FolderView;
import com.omid.notekeeper.ui.folder.FoldersActivity;
import com.omid.notekeeper.ui.notes.NotesActivity;
import com.omid.notekeeper.ui.notes.NotesView;

import javax.inject.Named;
import javax.inject.Qualifier;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/30/17.
 */
@Module
public class NotesActivityModule {

    private final NotesActivity notesActivity;

    public NotesActivityModule(NotesActivity notesActivity) {
        this.notesActivity = notesActivity;
    }

    @Provides
    @PerActivity
    public NotesView provideFolderView() {
        return notesActivity;
    }

}
