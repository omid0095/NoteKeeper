package com.omid.notekeeper.dagger.module;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.editor.EditorPresenter;
import com.omid.notekeeper.ui.editor.EditorView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/30/17.
 */
@Module
public class EditorActivityModule {

    private final EditorActivity editorActivity;

     public EditorActivityModule(EditorActivity editorActivity) {
        this.editorActivity = editorActivity;
    }

    @Provides
    @PerActivity
    public EditorView provideEditorView() {
        return editorActivity;
    }

    @Provides
    @PerActivity
    public EditorPresenter getPresenter(INoteStorage noteStorage, Note note, EditorView view) {
        return new EditorPresenter(noteStorage, note, view);
    }
}
