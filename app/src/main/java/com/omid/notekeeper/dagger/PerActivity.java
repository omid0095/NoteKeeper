package com.omid.notekeeper.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 *
 * Scope annotation for objects live in activity lifecycle.
 *
 * Created by Omid on 8/28/17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
