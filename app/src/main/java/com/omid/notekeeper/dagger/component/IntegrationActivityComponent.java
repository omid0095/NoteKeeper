package com.omid.notekeeper.dagger.component;

import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.dagger.module.EditorActivityModule;
import com.omid.notekeeper.dagger.module.IntegrationActivityModule;
import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.integration.IntegrationActivity;

import dagger.Component;

/**
 * Created by Omid on 8/30/17.
 */
@PerActivity
@Component(dependencies = DataComponent.class, modules = {IntegrationActivityModule.class})
public interface IntegrationActivityComponent {
    void inject(IntegrationActivity integrationActivity);
}
