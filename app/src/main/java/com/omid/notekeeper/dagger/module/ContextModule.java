package com.omid.notekeeper.dagger.module;

import android.content.Context;

import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/31/17.
 */
@Module
public class ContextModule {


    public ContextModule() {
    }

    @Provides
    @Singleton
    public NotesContext provideNotesContext() {
        return new NotesContext();
    }

    @Provides
    public Folder getCurrentFolder(NotesContext notesContext) {
        return notesContext.getCurrentFolder();
    }

    @Provides
    public Note getCurrentNote(NotesContext notesContext) {
        return notesContext.getCurrentNote();
    }
}
