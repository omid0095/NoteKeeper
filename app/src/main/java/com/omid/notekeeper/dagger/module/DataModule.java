package com.omid.notekeeper.dagger.module;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.omid.notekeeper.api.FileManager;
import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.InputValidator;
import com.omid.notekeeper.database.DatabaseHelper;
import com.omid.notekeeper.database.FolderEntity;
import com.omid.notekeeper.database.InputValidatorDBImp;
import com.omid.notekeeper.database.NoteEntity;
import com.omid.notekeeper.database.NoteStorageDBImp;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/30/17.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    DatabaseHelper provideDatabaseHelper(Context context) {
        return new DatabaseHelper(context);
    }

    @Provides
    @Singleton
    Dao<FolderEntity, Integer> provideFolderDao(DatabaseHelper databaseHelper) {
        return databaseHelper.getFolderDao();
    }

    @Provides
    @Singleton
    Dao<NoteEntity, Integer> provideNoteDao(DatabaseHelper databaseHelper) {
        return databaseHelper.getNoteDao();
    }

    @Provides
    @Singleton
    @Named("DB")
    String getDataBasePath(DatabaseHelper databaseHelper) {
        return databaseHelper.getWritableDatabase().getPath();
    }


    @Provides
    @Singleton
    InputValidator provideInputValidator(Context context, DatabaseHelper databaseHelper) {
        return new InputValidatorDBImp(context, databaseHelper);
    }

    @Provides
    @Singleton
    INoteStorage provideNoteStorage(Dao<FolderEntity, Integer> folderDao, Dao<NoteEntity, Integer> noteDao, InputValidator validator) {
        return new NoteStorageDBImp(folderDao, noteDao, validator);
    }
}
