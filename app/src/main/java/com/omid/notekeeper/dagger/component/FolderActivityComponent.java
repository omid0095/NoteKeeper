package com.omid.notekeeper.dagger.component;

import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.dagger.module.FolderActivityModule;
import com.omid.notekeeper.ui.folder.FoldersActivity;

import dagger.Component;

/**
 * Created by Omid on 8/30/17.
 */
@PerActivity
@Component(dependencies = DataComponent.class,modules = {FolderActivityModule.class})
public interface FolderActivityComponent {
    void inject(FoldersActivity foldersActivity);
}
