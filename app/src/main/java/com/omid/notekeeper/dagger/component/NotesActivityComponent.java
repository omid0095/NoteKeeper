package com.omid.notekeeper.dagger.component;

import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.dagger.module.FolderActivityModule;
import com.omid.notekeeper.dagger.module.NotesActivityModule;
import com.omid.notekeeper.ui.folder.FoldersActivity;
import com.omid.notekeeper.ui.notes.NotesActivity;

import dagger.Component;

/**
 * Created by Omid on 8/30/17.
 */
@PerActivity
@Component(dependencies = DataComponent.class,modules = {NotesActivityModule.class})
public interface NotesActivityComponent {
    void inject(NotesActivity notesActivity);
}
