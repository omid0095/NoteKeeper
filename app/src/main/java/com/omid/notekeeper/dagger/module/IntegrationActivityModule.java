package com.omid.notekeeper.dagger.module;

import com.omid.notekeeper.api.FileManager;
import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.ui.integration.IntegrationActivity;
import com.omid.notekeeper.ui.integration.IntegrationPresenter;
import com.omid.notekeeper.ui.integration.IntegrationView;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 8/30/17.
 */
@Module
public class IntegrationActivityModule {

    private final IntegrationActivity integrationActivity;
    private final int intigrateType;

    public IntegrationActivityModule(IntegrationActivity integrationActivity) {
        this.integrationActivity = integrationActivity;
        //type of integration is put into activity's intent.
        //We have to extract that.
        this.intigrateType = integrationActivity.getIntent().getIntExtra(IntegrationActivity.INTEGRATE_TYPE,0);
    }

    @Provides
    @PerActivity
    public IntegrationView provideIntegrationView() {
        return integrationActivity;
    }

    @Provides
    @PerActivity
    @Named("integrateType")
    public int getIntegrateType() {
        return intigrateType;
    }

    @Provides
    @PerActivity
    public IntegrationPresenter getPresenter(FileManager fileManager, IntegrationView view, @Named("integrateType") int integrateType, @Named("DB") String dbPath) {
        return new IntegrationPresenter(fileManager,view,integrateType,dbPath);
    }

    }
