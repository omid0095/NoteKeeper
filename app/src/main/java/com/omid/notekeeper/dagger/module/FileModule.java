package com.omid.notekeeper.dagger.module;

import com.omid.notekeeper.api.FileManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Omid on 9/1/17.
 */
@Module
public class FileModule {
    @Provides
    @Singleton
    FileManager getFileManager(){
        return new FileManager();
    }
}
