package com.omid.notekeeper.dagger.component;

import com.omid.notekeeper.api.FileManager;
import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.dagger.module.AppModule;
import com.omid.notekeeper.dagger.module.ContextModule;
import com.omid.notekeeper.dagger.module.DataModule;
import com.omid.notekeeper.dagger.module.FileModule;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

/**
 * The main singleton component of application
 * provides the data resolvers of the application.
 * Created by Omid on 8/30/17.
 */
@Singleton
@Component(modules = {AppModule.class, DataModule.class, ContextModule.class, FileModule.class})
public interface DataComponent {
    INoteStorage exportNoteStorage();

    NotesContext exportNotesContext();

    Folder exportFolder();

    Note exportNote();

    FileManager exportFileManager();

    /**
     * provides a path to import data into that, or export its data.
     * @return a path
     */
    @Named("DB")
    String exportDataBase();

}
