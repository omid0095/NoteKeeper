package com.omid.notekeeper.dagger.module;

import android.app.Activity;

import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.folder.FolderView;
import com.omid.notekeeper.ui.folder.FoldersActivity;
import com.omid.notekeeper.ui.integration.IntegrationActivity;
import com.omid.notekeeper.ui.notes.NotesActivity;

/**
 * The activity module provider
 * instance of this class can decide which Activity provider relates to each activity.
 * Created by Omid on 9/2/17.
 */

public class ActivityModuleProvider {
    public <T> T getActivityModule(Activity activity) {
        if (activity instanceof EditorActivity)
            return (T) new EditorActivityModule((EditorActivity) activity);

        if (activity instanceof FoldersActivity)
            return (T) new FolderActivityModule((FoldersActivity) activity);

        if (activity instanceof NotesActivity)
            return (T) new NotesActivityModule((NotesActivity) activity);

        if (activity instanceof IntegrationActivity)
            return (T) new IntegrationActivityModule((IntegrationActivity) activity);
        return null;
    }
}
