package com.omid.notekeeper.dagger.component;

import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.dagger.module.EditorActivityModule;
import com.omid.notekeeper.dagger.module.NotesActivityModule;
import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.notes.NotesActivity;

import dagger.Component;

/**
 * Created by Omid on 8/30/17.
 */
@PerActivity
@Component(dependencies = DataComponent.class, modules = {EditorActivityModule.class})
public interface EditorActivityComponent {
    void inject(EditorActivity editorActivity);
}
