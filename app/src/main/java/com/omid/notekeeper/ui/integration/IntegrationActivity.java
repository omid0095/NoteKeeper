package com.omid.notekeeper.ui.integration;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.omid.notekeeper.R;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerIntegrationActivityComponent;
import com.omid.notekeeper.dagger.module.IntegrationActivityModule;
import com.omid.notekeeper.ui.NoteKeeperActivity;
import com.omid.notekeeper.ui.integration.list.IntegrationAdapter;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class IntegrationActivity extends NoteKeeperActivity implements IntegrationView {
    public static final String INTEGRATE_TYPE = "integrateType";

    @Inject
    IntegrationPresenter presenter;

    @BindView(R.id.recyclerlist)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intigration);
        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //extract integration type from the intent
        int integrateType = getIntent().getIntExtra(INTEGRATE_TYPE, 0);

        NoteKeeperApplication app= (NoteKeeperApplication) getApplication();

        //create IntegrationActivity Component
        DaggerIntegrationActivityComponent.builder()
                .dataComponent(app.getComponent())
                .integrationActivityModule((IntegrationActivityModule) app.provideActivityModule(this))
                .build().inject(this);

        //injecting views using ButterKnife
        ButterKnife.bind(this);

        presenter.bind();

    }

    public void cancelClicked(View view) {
        close();
    }

    @Override
    public void showList(List<File> directories, List<File> noteFiles, boolean showSave) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        IntegrationAdapter adapter = new IntegrationAdapter(directories, noteFiles, showSave, presenter.getFIleClickListener());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showUpButton(Boolean show) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(show);
    }

    @Override
    public void setTitle(boolean isExport) {
        setTitle(isExport ? "Export" : "Import");
    }

    @Override
    public Observable<Boolean> askForReplace() {
        return askAQuestion("Replace", "Do you want replace backup with this file?");
    }

    @Override
    public Observable<String> askFileName() {
        return askForText("new File", "Enter Filename to export", "File name");
    }

    @Override
    public Observable<Void> showFileNameEmptyError() {
        return showMessage("Error", "Filename field shodn't be empty");
    }

    @Override
    public Observable<Void> showImportDone() {
        return showMessage("Done", "Importing is done successfully");
    }
    @Override
    public Observable<Void> showExportDone() {
        return showMessage("Done", "Exporting is done successfully");
    }

    @Override
    public void onBackPressed() {
        presenter.onBackClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            presenter.onUpClick();
        return super.onOptionsItemSelected(item);
    }
}
