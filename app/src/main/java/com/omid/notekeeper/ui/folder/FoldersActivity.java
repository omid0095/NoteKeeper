package com.omid.notekeeper.ui.folder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.omid.notekeeper.R;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerFolderActivityComponent;
import com.omid.notekeeper.dagger.module.FolderActivityModule;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.ui.NoteKeeperActivity;
import com.omid.notekeeper.ui.folder.list.FolderListAdapter;
import com.omid.notekeeper.ui.integration.IntegrationActivity;
import com.omid.notekeeper.ui.integration.IntegrationConstants;
import com.omid.notekeeper.ui.notes.NotesActivity;
import com.omid.notekeeper.view.InputDialogBuilder;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class FoldersActivity extends NoteKeeperActivity implements FolderView {

    @Inject
    FolderPresenter presenter;

    @BindView(R.id.recyclerlist)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folders);
        initToolbar();
        NoteKeeperApplication app = ((NoteKeeperApplication) getApplication());

        //create FolderActivityComponent
        DaggerFolderActivityComponent.builder()
                .dataComponent(app.getComponent())
                .folderActivityModule((FolderActivityModule) app.provideActivityModule(this))
                .build().inject(this);

        //inject views using Butterknife
        ButterKnife.bind(this);

        initView();

    }

    private void initView() {
        setTitle("Folders");
    }

    @Override
    public void showFolders(List<Folder> folders) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        FolderListAdapter folderListAdapter = new FolderListAdapter(folders, presenter.getFolderClickListener());
        recyclerView.setAdapter(folderListAdapter);
    }

    @Override
    public void notifyFolderAdd(Folder folder) {
        ((FolderListAdapter) recyclerView.getAdapter()).addFolder(folder);
    }

    @Override
    public void notifyFolderRemove(Folder folder) {
        ((FolderListAdapter) recyclerView.getAdapter()).removeFolder(folder);
    }

    @Override
    public void openFolder() {
        Intent intent = new Intent(this, NotesActivity.class);
        startActivity(intent);
    }

    private void openIntegration(int integrateType) {
        Intent intent = new Intent(this, IntegrationActivity.class);
        //put the integration type in the intent and, send it.
        intent.putExtra(IntegrationActivity.INTEGRATE_TYPE, integrateType);
        startActivity(intent);
    }

    @Override
    public Observable<Void> showError(String message) {
        return showMessage("Error", message);
    }

    @Override
    public Observable<Boolean> askForDelete() {
        return askAQuestion("Delete", "Are You Sure?");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_folders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_folder_add)
            onAddButtonClick();
        if (item.getItemId() == R.id.menu_folder_export)
            openIntegration(IntegrationConstants.EXPORT);
        if (item.getItemId() == R.id.menu_folder_import)
            openIntegration(IntegrationConstants.IMPORT);

        return super.onOptionsItemSelected(item);
    }

    private void onAddButtonClick() {
        askForText("New Folder", "Enter new Folder name:", "Name").subscribe(new Consumer<String>() {
            @Override
            public void accept(@NonNull String s) throws Exception {
                presenter.createNewFolder(s);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unSubscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadFolders();
    }
}
