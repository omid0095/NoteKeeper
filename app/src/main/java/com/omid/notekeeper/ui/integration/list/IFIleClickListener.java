package com.omid.notekeeper.ui.integration.list;

import java.io.File;

/**
 * Created by Omid on 9/1/17.
 */

public interface IFIleClickListener {

    void onDirectoryClick(File directory);

    void onNoteFileClick(File noteFile);

    void onSaveHereClick();
}
