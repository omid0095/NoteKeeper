package com.omid.notekeeper.ui.notes;


import com.j256.ormlite.stmt.query.Not;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.IBaseView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Omid on 8/30/17.
 */

public interface NotesView extends IBaseView {

    void showNotes(List<Note> notes);

    void notifyNoteAdd(Note note);

    void openNote();

    Observable<Void> showError(String message);
}
