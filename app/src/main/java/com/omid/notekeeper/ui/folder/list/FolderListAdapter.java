package com.omid.notekeeper.ui.folder.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omid.notekeeper.R;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.ui.SimpleViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * This class adapts a list of folders in a Recycler List
 * Created by Omid on 8/30/17.
 */

public class FolderListAdapter extends RecyclerView.Adapter {

    private static final int TYPE_FOLDER = 1;
    private static final int TYPE_EMPTY = 2;
    private final List<Folder> folders;
    private final IFolderClickListener listener;

    public FolderListAdapter(List<Folder> folders, IFolderClickListener listener) {
        this.folders = new ArrayList<>(folders);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        //if there is no folder so we have a no-folder item
        if (folders.size() == 0)
            return TYPE_EMPTY;
        else
            return TYPE_FOLDER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOLDER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_folder, parent, false);
            return new FolderViewHolder(v);
        } else {    // else it is a no-folder
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_no_folder, parent, false);
            return new SimpleViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FolderViewHolder)
            ((FolderViewHolder) holder).bind(folders.get(position), listener);
    }

    @Override
    public int getItemCount() {
        //if there is no folder so we have one no-folder item
        if (folders.size() == 0)
            return 1;
        else
            return folders.size();
    }

    public void addFolder(Folder folder) {
        folders.add(folder);
        notifyDataSetChanged();
        // TODO: 8/30/17 use notifyItemInsert
    }

    public void removeFolder(Folder folder) {
        int index = folders.indexOf(folder);
        folders.remove(index);
        notifyDataSetChanged();
        // TODO: 8/30/17 use notifyItemRemoved
    }
}
