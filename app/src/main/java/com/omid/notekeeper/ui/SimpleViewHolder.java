package com.omid.notekeeper.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Omid on 8/30/17.
 */

public class SimpleViewHolder extends RecyclerView.ViewHolder {
    public SimpleViewHolder(View itemView) {
        super(itemView);
    }
}
