package com.omid.notekeeper.ui.integration.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omid.notekeeper.R;
import com.omid.notekeeper.ui.SimpleViewHolder;

import java.io.File;
import java.util.List;

/**
 * this class adapts a list of directory and a list of file for a recycler view
 * Created by Omid on 9/1/17.
 */

public class IntegrationAdapter extends RecyclerView.Adapter {


    private static final int TYPE_DIRECTORY = 1;
    private static final int TYPE_NOTEFILE = 2;
    private static final int TYPE_SAVE_HERE = 3;
    private static final int TYPE_EMPTY = 4;


    private final List<File> directories;
    private final List<File> noteFiles;
    private final boolean showSaveHere;
    private final IFIleClickListener listener;

    public IntegrationAdapter(List<File> directories, List<File> noteFiles, boolean showSaveHere, IFIleClickListener listener) {
        this.directories = directories;
        this.noteFiles = noteFiles;
        this.showSaveHere = showSaveHere;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_DIRECTORY) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_directory, parent, false);
            return new DirectoryViewHolder(v);

        } else if (viewType == TYPE_NOTEFILE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_notefile, parent, false);
            return new NoteFileViewHolder(v);

        } else if (viewType == TYPE_SAVE_HERE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_save_here, parent, false);
            return new SaveHereViewHolder(v);

        } else {    //empty state
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_directory_empty, parent, false);
            return new SimpleViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DirectoryViewHolder)
            ((DirectoryViewHolder) holder).bind(directories.get(position), listener);

        if (holder instanceof NoteFileViewHolder) {
            //reduce count of directories from the position to get real position in files
            int filePos = position - directories.size();
            ((NoteFileViewHolder) holder).bind(noteFiles.get(filePos), listener);
        }

        if (holder instanceof SaveHereViewHolder)
            ((SaveHereViewHolder) holder).bind(listener);
    }

    @Override
    public int getItemCount() {
        //first calculate notes and directories count
        int count = directories.size() + noteFiles.size();

        //then if adapter has to show "SaveHere" involve that.
        if (showSaveHere)
            count++;

        //finally if still count is zero, we have a empty-state view
        if (count == 0)    //show empty state
            count = 1;

        return count;
    }

    @Override
    public int getItemViewType(int position) {
        //position for the directory is less than directories size.
        if (position < directories.size())
            return TYPE_DIRECTORY;

        //position for the file is less than sum of directories and files
        else if (position < directories.size() + noteFiles.size())
            return TYPE_NOTEFILE;

        //if not, check if we have to show "SaveHere"
        else if (showSaveHere)
            return TYPE_SAVE_HERE;

        //and if not, be sure it is empty-state type
        else
            return TYPE_EMPTY;
    }
}
