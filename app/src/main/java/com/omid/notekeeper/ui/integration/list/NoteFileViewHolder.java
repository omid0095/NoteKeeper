package com.omid.notekeeper.ui.integration.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.omid.notekeeper.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Omid on 9/1/17.
 */

public class NoteFileViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text)
    TextView textView;
    @BindView(R.id.clickable)
    View clickable;
    public NoteFileViewHolder(View v) {
        super(v);
        ButterKnife.bind(this,v);
    }

    public void bind(final File noteFile, final IFIleClickListener listener) {
        textView.setText(noteFile.getName());
        clickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNoteFileClick(noteFile);
            }
        });
    }
}
