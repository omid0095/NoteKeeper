package com.omid.notekeeper.ui.editor;

import android.graphics.Bitmap;

import com.omid.notekeeper.ui.IBaseView;
import com.omid.notekeeper.ui.editor.drawer.DrawerView;
import com.omid.notekeeper.ui.editor.richtext.RichTextView;

/**
 * Created by Omid on 8/31/17.
 */

public interface EditorView extends IBaseView {

    void setRichText(String string);

    String getRichText();

    void setBitmap(Bitmap bitmap);

    Bitmap getBitmap();

    void showSave(Boolean enabled);


    void leaveWithoutSave();
}
