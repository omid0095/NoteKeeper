package com.omid.notekeeper.ui.notes.list;

import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

/**
 * Created by Omid on 8/30/17.
 */

public interface INotesClickListener {

    void onNoteClick(Note note);
}
