package com.omid.notekeeper.ui.integration;

import com.omid.notekeeper.app.Constants;
import com.omid.notekeeper.api.FileManager;
import com.omid.notekeeper.ui.integration.list.IFIleClickListener;

import java.io.File;
import java.util.List;

import javax.inject.Named;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Created by Omid on 8/31/17.
 */

public class IntegrationPresenter {

    private final FileManager fileManager;
    private final IntegrationView view;
    private final String dbPath;
    private final boolean isExport;
    private IFIleClickListener fileClickListener;
    private File currentDirectory;


    public IntegrationPresenter(FileManager fileManager, IntegrationView view, @Named("integrateType") int integrateType, @Named("DB") String dbPath) {
        this.fileManager = fileManager;
        this.view = view;
        this.dbPath = dbPath;
        this.isExport = integrateType == IntegrationConstants.EXPORT;
    }

    public void bind() {
        view.setTitle(isExport);
        showDirectory(fileManager.getRootDirectory());

    }

    private void showDirectory(File directory) {
        currentDirectory = directory;
        updateView();
        List<File> directories = fileManager.getDirectories(directory);
        List<File> noteFiles = fileManager.getNoteFiles(directory);
        view.showList(directories, noteFiles, isExport);
    }

    private void updateView() {
        //if we are showing root directory, ask view to hide the up button
        if (isRoot()) {
            view.showUpButton(false);
            view.setSecondaryTitle("Phone Storage");
        } else {
            view.showUpButton(true);
            view.setSecondaryTitle(currentDirectory.getName());
        }
    }

    private boolean isRoot() {
        return currentDirectory.getPath().equals(fileManager.getRootDirectory().getPath());
    }

    public IFIleClickListener getFIleClickListener() {
        if (fileClickListener == null)
            fileClickListener = new IFIleClickListener() {
                @Override
                public void onDirectoryClick(File directory) {
                    showDirectory(directory);
                }

                @Override
                public void onNoteFileClick(final File noteFile) {

                    //if it is export mode ask the view for replacing backup with selected file
                    if (isExport)
                        view.askForReplace().subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(@NonNull Boolean aBoolean) throws Exception {
                                if (aBoolean)
                                    exportTo(noteFile.getPath());
                            }
                        });
                    else
                        //if it is import mode, just import from selected file
                        importFrom(noteFile.getPath());
                }

                @Override
                public void onSaveHereClick() {
                    view.askFileName().subscribe(new Consumer<String>() {
                        @Override
                        public void accept(@NonNull String s) throws Exception {
                            exportTo(currentDirectory.getPath(), s);
                        }
                    });
                }
            };
        return fileClickListener;
    }

    private void exportTo(String path, String name) {

        //check if name is empty tell view to show error

        if (name.equals(""))
            view.showFileNameEmptyError().subscribe();

        //make a path base on the path and entered name
        String newPath = path + "/" + name + "." + Constants.NOTE_FILE_EXT;
        exportTo(newPath);
    }

    private void importFrom(String path) {
        //if importing is done notify view
        if (fileManager.copy(path, dbPath)) {
            view.showImportDone().doOnComplete(new Action() {
                @Override
                public void run() throws Exception {
                    view.close();

                }
            }).subscribe();
        }
    }

    private void exportTo(String path) {
        //if exporting is done notify view

        if (fileManager.copy(dbPath, path)) {
            view.showExportDone().doOnComplete(new Action() {
                @Override
                public void run() throws Exception {
                    view.close();
                }
            }).subscribe();
        }
    }

    public void onBackClick() {
        if (isRoot())
            view.close();
        else
            onUpClick();
    }

    public void onUpClick() {
        if (!isRoot())
            showDirectory(currentDirectory.getParentFile());
    }
}
