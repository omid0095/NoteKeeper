package com.omid.notekeeper.ui.editor.richtext;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.omid.notekeeper.ui.editor.EditorView;
import com.omid.notekeeper.ui.editor.drawer.DrawerView;
import com.omid.notekeeper.util.SystemSize;

import jp.wasabeef.richeditor.RichEditor;

/**
 * This View wraps a RichTextView, all of complexities of working
 * with RichTextView are here
 * Created by Omid on 8/31/17.
 */

public class RichTextView {
    public interface Listener {

        void onTextChanged(RichTextView richTextView);

        void onEditorSelected(RichTextView richTextView);
    }

    private final Listener listener;
    private final RichEditor view;


    public RichTextView(RichEditor view, Listener listener) {
        this.view = view;
        this.listener = listener;
        setupOnTextChange();
        setupOnSelect();
        view.focusEditor();
        init();

    }

    private void init() {
//        innitialize the view, default size by dp unit.
        view.setEditorFontSize(22);
        view.setPadding(8, 8, 8, 8);
    }

    private void setupOnSelect() {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //on touch view fire the view is selected.
                listener.onEditorSelected(RichTextView.this);
                return false;
            }
        });
    }

    private void setupOnTextChange() {
        view.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                //on text change fire the text is changed
                listener.onTextChanged(RichTextView.this);
            }
        });
    }

    public void setRichText(String string) {
        if ((string != null))
            view.setHtml(string);
        else
            view.setHtml("");
    }

    public String getRichText() {
        return view.getHtml();
    }

    public void setTextBold() {
        view.setBold();
        //style changed so call the listener
        listener.onTextChanged(RichTextView.this);

    }

    public void setTextItalic() {
        view.setItalic();
        //style changed so call the listener
        listener.onTextChanged(RichTextView.this);

    }

    public void disableEditing() {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(view.getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void enableEditing() {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);

    }

    public void hide() {
        view.clearFocusEditor();
        view.setVisibility(View.GONE);
    }

    public void show() {
        view.setVisibility(View.VISIBLE);
    }

}
