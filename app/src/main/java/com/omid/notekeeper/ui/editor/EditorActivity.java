package com.omid.notekeeper.ui.editor;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.omid.notekeeper.R;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerEditorActivityComponent;
import com.omid.notekeeper.dagger.module.EditorActivityModule;
import com.omid.notekeeper.ui.NoteKeeperActivity;
import com.omid.notekeeper.ui.editor.drawer.DrawerView;
import com.omid.notekeeper.ui.editor.richtext.RichTextView;
import com.omid.notekeeper.view.DrawingView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import jp.wasabeef.richeditor.RichEditor;

public class EditorActivity extends NoteKeeperActivity implements EditorView {

    @Inject
    EditorPresenter presenter;
    private DrawerView drawerView;
    private RichTextView richTextView;
    @BindView(R.id.ok_fab)
    FloatingActionButton okFab;
    @BindView(R.id.menu_fab)
    FloatingActionButton menuFab;
    @BindView(R.id.bold_fab)
    FloatingActionButton boldFab;
    @BindView(R.id.italic_fab)
    FloatingActionButton italicFab;
    @BindView(R.id.pen_fab)
    FloatingActionButton penFab;

    private boolean menuOpen;
    private MenuItem saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NoteKeeperApplication app = (NoteKeeperApplication) getApplication();

        //create EditorActivity Component.
        DaggerEditorActivityComponent.builder()
                .dataComponent(app.getComponent())
                .editorActivityModule((EditorActivityModule) app.provideActivityModule(this))
                .build().inject(this);

        //inject views using butterKnife
        ButterKnife.bind(this);

        //initiallizing views
        initDrawerView();
        initRichTextView();

        //ask presenter to bind view
        presenter.bindView();
    }

    private void initRichTextView() {
        richTextView = new RichTextView((RichEditor) findViewById(R.id.richtext), new RichTextView.Listener() {
            @Override
            public void onTextChanged(RichTextView richTextView) {
                presenter.onRichTextChanged();
            }

            @Override
            public void onEditorSelected(RichTextView richTextView) {
                closeMenu();
            }
        });

    }

    private void initDrawerView() {
        drawerView = new DrawerView((DrawingView) findViewById(R.id.drawer), new DrawerView.Listener() {
            @Override
            public void onDraw(DrawerView drawerView) {
                presenter.onDrawerChanged();
            }
        });

    }

    @Override
    public void setRichText(String string) {
        richTextView.setRichText(string);
    }

    @Override
    public String getRichText() {
        return richTextView.getRichText();
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        drawerView.setBitmap(bitmap);
    }

    @Override
    public Bitmap getBitmap() {
        return drawerView.getBitmap();
    }

    @Override
    public void showSave(Boolean enabled) {
        saveButton.setVisible(enabled);
    }

    @Override
    public void leaveWithoutSave() {
        askAQuestion("Leave", "Are you going to leave without saving?").subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(@NonNull Boolean aBoolean) throws Exception {
                if (aBoolean)
                    close();
            }
        });
    }


    private void closeDrawer() {
        okFab.hide();
        menuFab.show();
        richTextView.show();
        drawerView.disableEditing();
    }

    private void openDrawer() {
        okFab.show();
        richTextView.hide();
        drawerView.enableEditing();
    }

    private void openMenu() {
        menuFab.setImageResource(R.drawable.collapse_24);

        richTextView.disableEditing();

        penFab.show();
        boldFab.show();
        italicFab.show();
        menuOpen = true;
    }

    private void closeMenu() {
        menuFab.setImageResource(R.drawable.edit_24);
        penFab.hide();
        boldFab.hide();
        italicFab.hide();
        menuOpen = false;
    }

    private void hideMenu() {
        closeMenu();
        menuFab.hide();
    }


    private boolean isMenuOpen() {
        return menuOpen;
    }

    public void showMenu() {
        menuFab.show();
    }

    public void okFabOnClick(View view) {
        closeDrawer();
        showMenu();
    }

    public void italicFabOnClick(View view) {
        closeMenu();
        richTextView.setTextItalic();
        richTextView.enableEditing();
    }

    public void boldFabOnClick(View view) {
        closeMenu();
        richTextView.setTextBold();
        richTextView.enableEditing();
    }

    public void penFabOnClick(View view) {
        hideMenu();
        openDrawer();
    }

    public void menuFabOnclick(View view) {
        if (isMenuOpen())
            closeMenu();
        else
            openMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        saveButton = menu.findItem(R.id.menu_note_save);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_note_save)
            presenter.onSaveClick();
        if (item.getItemId() == R.id.menu_note_delete)
            onDeleteButtonClick();
        if (item.getItemId() == android.R.id.home)
            presenter.onUpCLick();
        return super.onOptionsItemSelected(item);
    }

    private void onDeleteButtonClick() {
        askAQuestion("Delete", "Are you sure?").subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(@NonNull Boolean aBoolean) throws Exception {
                if (aBoolean)
                    presenter.onDeleteConfirmed();

            }
        });
    }

    @Override
    public void onBackPressed() {
        presenter.onBackClick();
    }
}
