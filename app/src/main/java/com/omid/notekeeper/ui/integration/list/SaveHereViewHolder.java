package com.omid.notekeeper.ui.integration.list;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.omid.notekeeper.R;
import com.omid.notekeeper.ui.folder.list.IFolderClickListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Omid on 9/1/17.
 */

public class SaveHereViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.clickable)
    View clickable;
    @BindView(R.id.text)
    TextView textView;

    public SaveHereViewHolder(View v) {
        super(v);
        ButterKnife.bind(this, v);
        underlineText();
    }

    private void underlineText() {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void bind(final IFIleClickListener listener) {
        clickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSaveHereClick();
            }
        });
    }
}
