package com.omid.notekeeper.ui.editor;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.model.Note;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Omid on 8/31/17.
 */

public class EditorPresenter {


    private final INoteStorage noteStorage;
    private final Note note;
    private final EditorView view;
    private boolean changed = false;

    public EditorPresenter(INoteStorage noteStorage, Note note, EditorView view) {
        this.noteStorage = noteStorage;
        this.note = note;
        this.view = view;

    }

    public void bindView() {
        //view's titile should be the note's name
        view.setTitle(note.getName());
        view.setRichText(note.getString());
        view.setBitmap(note.getBitmap());
    }


    public void onRichTextChanged() {
        changed = true;
        view.showSave(true);
    }


    public void onDrawerChanged() {
        changed = true;
        view.showSave(true);
    }

    public void onSaveClick() {
        //get the note's latest changes from the view
        note.setBitmap(view.getBitmap());
        note.setString(view.getRichText());
        //ask the storage to save the note
        noteStorage.saveNote(note).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        changed = false;
                        //and finally ask view to hide the save button
                        view.showSave(false);
                    }
                }).subscribe();
    }

    public void onUpCLick() {
        onBackClick();
    }

    public void onBackClick() {
        if (changed)
            view.leaveWithoutSave();
        else
            view.close();
    }

    public void onDeleteConfirmed() {
        noteStorage.deleteNote(note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        view.close();
                    }
                }).subscribe();
    }

    ;
}
