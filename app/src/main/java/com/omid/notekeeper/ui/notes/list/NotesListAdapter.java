package com.omid.notekeeper.ui.notes.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omid.notekeeper.R;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.SimpleViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Omid on 8/30/17.
 */

public class NotesListAdapter extends RecyclerView.Adapter {

    private static final int TYPE_NOTE = 1;
    private static final int TYPE_EMPTY = 2;
    private final List<Note> notes;
    private final INotesClickListener listener;

    public NotesListAdapter(List<Note> notes, INotesClickListener listener) {
        this.notes = new ArrayList<>(notes);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        // if notes is empty so we have to pass the empty-type
        if (notes.size() == 0)
            return TYPE_EMPTY;
        else
            return TYPE_NOTE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_NOTE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_note, parent, false);
            return new NotesViewHolder(v);

        } else {    // else there is no notes.
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_no_note, parent, false);
            return new SimpleViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotesViewHolder)
            ((NotesViewHolder) holder).bind(notes.get(position), listener);
    }

    @Override
    public int getItemCount() {
        if (notes.size() == 0)
            return 1;
        else
            return notes.size();
    }

    public void addNote(Note note) {
        notes.add(note);
        notifyDataSetChanged();
        // TODO: 8/30/17 use notifyItemInsert
    }

    public void removeNote(Note note) {
        int index = notes.indexOf(note);
        notes.remove(index);
        notifyDataSetChanged();
        // TODO: 8/30/17 use notifyItemRemoved
    }
}
