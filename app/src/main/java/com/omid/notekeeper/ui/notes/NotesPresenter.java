package com.omid.notekeeper.ui.notes;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.dagger.PerActivity;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.notes.list.INotesClickListener;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * this class presents Note Storages data, for NotesView
 * Created by Omid on 8/30/17.
 */
@PerActivity
public class NotesPresenter {

    private final INoteStorage noteStorage;
    private final NotesView view;
    private final NotesContext notesContext;
    private final Folder currentFolder;
    private INotesClickListener noteClickListener;
    private Disposable subscriber;

    @Inject
    public NotesPresenter(Folder currentFolder, NotesContext notesContext, INoteStorage noteStorage, NotesView view) {
        this.notesContext = notesContext;
        this.currentFolder = currentFolder;
        this.noteStorage = noteStorage;
        this.view = view;
    }

    public void loadNotes() {
        view.setSecondaryTitle(currentFolder.getName());
        subscriber = noteStorage.getNotes(currentFolder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Note>>() {
                    @Override
                    public void accept(@NonNull List<Note> notes) throws Exception {
                        view.showNotes(notes);
                    }
                });
    }

    public void unSubscribe() {
        if (subscriber != null && (!subscriber.isDisposed()))
            subscriber.dispose();
    }

    public INotesClickListener getNoteCLickListener() {
        if (noteClickListener == null)
            noteClickListener = new INotesClickListener() {
                @Override
                public void onNoteClick(Note note) {
                    openNote(note);
                }
            };
        return noteClickListener;
    }

    private void openNote(Note note) {
        notesContext.setCurrentNote(note);
        view.openNote();
    }


    public void createNewNote(String name) {

        //ask storage to create a new note
        noteStorage.addNote(currentFolder, name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Note>() {
                    @Override
                    public void accept(@NonNull Note note) throws Exception {
                        //notify user that a note is added
                        view.notifyNoteAdd(note);

                        //and then open the note
                        openNote(note);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        view.showError(throwable.getMessage()).subscribe();
                    }
                });
    }

    public void onUpClick() {
        unSubscribe();
        view.close();
    }
}
