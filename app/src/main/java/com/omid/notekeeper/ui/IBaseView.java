package com.omid.notekeeper.ui;

import io.reactivex.Observable;

/**
 * an interface of base view which does basic things that
 * every View has to do.
 * Created by Omid on 8/30/17.
 */

public interface IBaseView {


    /**
     * show a simple message
     * @param title title of message
     * @param msg text of message
     * @return obserable that notifies caller that user dismissed the message
     */
    Observable<Void> showMessage(String title, String msg);

    /**
     * set the title of view
     */
    void setTitle(String title);

    /**
     * a baseView object has to show a secondary title
     */
    void setSecondaryTitle(String title);

    /**
     * ask a yes/no question from the user and notify the caller by the answer
     * @param title title of question
     * @param msg   text of question
     * @return  an observable of boolean
     */
    Observable<Boolean> askAQuestion(String title, String msg);

    /**
     * ask view for a text
     * @param title title of message
     * @param msg text of message
     * @param hint a hint for input text
     * @return an observable of string
     */
    Observable<String> askForText(String title, String msg, String hint);

    /**
     * each base view has to have a close option
     */
    void close();
}
