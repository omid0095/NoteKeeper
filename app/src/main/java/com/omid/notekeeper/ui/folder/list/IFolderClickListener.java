package com.omid.notekeeper.ui.folder.list;

import com.omid.notekeeper.model.Folder;

/**
 * Created by Omid on 8/30/17.
 */

public interface IFolderClickListener {

    void onFolderClick(Folder folder);
    void onFolderDeleteClick(Folder folder);
}
