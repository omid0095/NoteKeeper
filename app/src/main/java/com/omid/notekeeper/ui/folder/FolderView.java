package com.omid.notekeeper.ui.folder;


import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.ui.IBaseView;
import com.omid.notekeeper.view.InputDialogBuilder;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Omid on 8/30/17.
 */

public interface FolderView extends IBaseView {
    void showFolders(List<Folder> folders);

    void notifyFolderAdd(Folder folder);

    void notifyFolderRemove(Folder folder);

    void openFolder();

    Observable<Void> showError(String message);

    Observable<Boolean> askForDelete();
}
