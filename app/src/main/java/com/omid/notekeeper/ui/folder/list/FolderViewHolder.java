package com.omid.notekeeper.ui.folder.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.omid.notekeeper.R;
import com.omid.notekeeper.model.Folder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Holds the view of a folder type, and bind different folders in the view
 * Created by Omid on 8/30/17.
 */

public class FolderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.folder_name)
    TextView nameView;
    @BindView(R.id.folder_description)
    TextView descriptionView;
    @BindView(R.id.clickable)
    View clickable;
    @BindView(R.id.folder_delete)
    View deleteBtn;

    public FolderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bind(final Folder folder, final IFolderClickListener listener) {

        nameView.setText(folder.getName() != null ? folder.getName() : "");
        String description;
        if (folder.getContentsCount() == 0)
            description = "No Files";
        else if (folder.getContentsCount() == 1)
            description = "One File";
        else
            description = folder.getContentsCount() + " Files";
        descriptionView.setText(description);
        clickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFolderClick(folder);
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFolderDeleteClick(folder);
            }
        });

    }
}
