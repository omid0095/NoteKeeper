package com.omid.notekeeper.ui.notes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.omid.notekeeper.R;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerNotesActivityComponent;
import com.omid.notekeeper.dagger.module.NotesActivityModule;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.NoteKeeperActivity;
import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.folder.list.FolderListAdapter;
import com.omid.notekeeper.ui.notes.list.NotesListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class NotesActivity extends NoteKeeperActivity implements NotesView {

    @Inject
    NotesPresenter presenter;
    @BindView(R.id.recyclerlist)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        initToolbar();

        NoteKeeperApplication app = (NoteKeeperApplication) getApplication();

        //create NoteActivity Component
        DaggerNotesActivityComponent.builder()
                .dataComponent(app.getComponent())
                .notesActivityModule((NotesActivityModule) app.provideActivityModule(this))
                .build().inject(this);

        //inject views via ButterKnife
        ButterKnife.bind(this);

        initView();

        presenter.loadNotes();
    }

    private void initView() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Notes");
    }

    @Override
    public void showNotes(List<Note> notes) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        NotesListAdapter notesListAdapter = new NotesListAdapter(notes, presenter.getNoteCLickListener());
        recyclerView.setAdapter(notesListAdapter);
    }

    @Override
    public void notifyNoteAdd(Note note) {
        ((NotesListAdapter) recyclerView.getAdapter()).addNote(note);
    }

    @Override
    public void openNote() {
        Intent intent = new Intent(this, EditorActivity.class);
        startActivity(intent);

    }

    @Override
    public Observable<Void> showError(String message) {
        return showMessage("Error", message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            presenter.onUpClick();
        if (item.getItemId() == R.id.menu_note_add)
            onAddButtonClick();
        return super.onOptionsItemSelected(item);

    }

    private void onAddButtonClick() {
        askForText("New Note", "Enter new Note name:", "Name").subscribe(new Consumer<String>() {
            @Override
            public void accept(@NonNull String s) throws Exception {
                presenter.createNewNote(s);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unSubscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadNotes();
    }

}
