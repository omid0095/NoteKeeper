package com.omid.notekeeper.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.omid.notekeeper.R;
import com.omid.notekeeper.ui.folder.FoldersActivity;
import com.omid.notekeeper.view.InputDialogBuilder;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * an abstract implementation of BaseView
 * the methods are implemented with AlertDialog
 * Created by Omid on 8/30/17.
 */

public abstract class NoteKeeperActivity extends AppCompatActivity implements IBaseView {
    private Toolbar toolbar;


    @Override
    public Observable<Void> showMessage(final String title, final String msg) {
        return Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<Void> e) throws Exception {
                new AlertDialog.Builder(NoteKeeperActivity.this, R.style.dialogTheme).setTitle(title).setMessage(msg).setPositiveButton(R.string.input_dlg_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        e.onComplete();
                    }
                }).create().show();
            }
        });
    }


    @Override
    public Observable<Boolean> askAQuestion(final String title, final String msg) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<Boolean> e) throws Exception {
                new AlertDialog.Builder(NoteKeeperActivity.this, R.style.dialogTheme).setPositiveButton(R.string.input_dlg_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        e.onNext(true);
                        e.onComplete();
                        dialog.dismiss();
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        e.onNext(false);
                        e.onComplete();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.input_dlg_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setTitle(title).setMessage(msg).create().show();
            }
        });
    }

    @Override
    public Observable<String> askForText(final String title, final String msg, final String hint) {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<String> e) throws Exception {
                new InputDialogBuilder(NoteKeeperActivity.this, new InputDialogBuilder.InputDialogListener() {
                    @Override
                    public void onOKPressed(String text) {
                        e.onNext(text);
                    }
                }).setHint(hint)
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                e.onComplete();
                            }
                        }).setTitle(title).setMessage(msg)
                        .create().show();
            }
        });

    }

    @Override
    public void close() {
        finish();
    }

    protected void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void setTitle(String title) {
        TextView bigTitle = (TextView) toolbar.findViewById(R.id.toolbar_bigtitle);
        bigTitle.setText(title);
    }

    @Override
    public void setSecondaryTitle(String title) {
        TextView smallTitle = (TextView) toolbar.findViewById(R.id.toolbar_smalltitle);
        smallTitle.setText(title);
        if (title == null || title.equals(""))
            smallTitle.setVisibility(View.GONE);
        else
            smallTitle.setVisibility(View.VISIBLE);
    }
}
