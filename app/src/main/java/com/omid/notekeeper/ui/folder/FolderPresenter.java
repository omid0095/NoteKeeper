package com.omid.notekeeper.ui.folder;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.ui.folder.list.IFolderClickListener;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * The connection between NoteStorage and a FolderView
 *
 * Created by Omid on 8/30/17.
 */
public class FolderPresenter {

    private final INoteStorage noteStorage;
    private final FolderView view;
    private final NotesContext notesContext;
    private Disposable subscriber;
    private IFolderClickListener folderClickListener;

    public FolderPresenter(INoteStorage noteStorage, NotesContext notesContext, FolderView view) {
        this.noteStorage = noteStorage;
        this.notesContext = notesContext;
        this.view = view;
    }

    public void loadFolders() {
        subscriber = noteStorage.getFolders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Folder>>() {
                    @Override
                    public void accept(@NonNull List<Folder> folders) throws Exception {
                        view.showFolders(folders);
                    }

                });
    }

    public void unSubscribe() {
        if (subscriber != null && (!subscriber.isDisposed()))
            subscriber.dispose();
    }

    public IFolderClickListener getFolderClickListener() {
        if (folderClickListener == null)
            folderClickListener = new IFolderClickListener() {
                @Override
                public void onFolderClick(Folder folder) {
                    openFolder(folder);
                }

                @Override
                public void onFolderDeleteClick(final Folder folder) {
                    view.askForDelete().subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(@NonNull Boolean delete) throws Exception {
                            if (delete)
                                deleteFolder(folder);
                        }
                    });
                }
            };
        return folderClickListener;

    }

    private void openFolder(Folder folder) {
        //save the folder as current folder in NotesContext,
        //and then ask view to open the folder
        notesContext.setCurrentFolder(folder);
        view.openFolder();
    }

    private void deleteFolder(final Folder folder) {
        //ask the storage to delete the folder
        noteStorage.deleteFolder(folder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        //when noteStorage answers that deleting is done,
                        //its time to tell the view to remove the folder
                        view.notifyFolderRemove(folder);
                    }
                }).subscribe();
    }

    public void createNewFolder(String name) {
        noteStorage.addFolder(name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Folder>() {
                    @Override
                    public void accept(@NonNull Folder folder) throws Exception {
                        view.notifyFolderAdd(folder);
                        openFolder(folder);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        view.showError(throwable.getMessage()).subscribe();
                    }
                });
    }
}
