package com.omid.notekeeper.ui.notes.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.omid.notekeeper.R;
import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by Omid on 8/30/17.
 */

public class NotesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.note_name)
    TextView nameView;
    @BindView(R.id.clickable)
    View clickable;


    public NotesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    public void bind(final Note note, final INotesClickListener listener) {

        nameView.setText(note.getName() != null ? note.getName() : "");
        clickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNoteClick(note);
            }
        });

    }
}
