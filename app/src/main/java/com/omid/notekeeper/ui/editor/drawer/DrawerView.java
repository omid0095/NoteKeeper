package com.omid.notekeeper.ui.editor.drawer;

import android.graphics.Bitmap;

import com.omid.notekeeper.R;
import com.omid.notekeeper.view.DrawingView;

/**
 * A View of DrawingView, it wraps a DrawingView object
 * Created by Omid on 8/31/17.
 */

public class DrawerView {
    private final Listener listener;

    public interface Listener {

        void onDraw(DrawerView drawerView);
    }

    private final DrawingView view;


    public DrawerView(DrawingView view, Listener listener) {
        this.view = view;
        this.listener = listener;
        initView();
        setupOnChangeListener();
    }

    private void initView() {
    }

    private void setupOnChangeListener() {
        view.setOnDrawListener(new DrawingView.onDrawListener() {
            @Override
            public void onDraw(DrawingView drawingView) {
                listener.onDraw(DrawerView.this);
            }
        });
    }

    public void setBitmap(Bitmap bitmap) {
        if (bitmap != null)
            view.setmBitmap(bitmap);
    }

    public Bitmap getBitmap() {
        return view.getmBitmap();
    }

    public void enableEditing() {
        //show a frame behind the view to make user focus on the view.
        view.setBackgroundResource(R.drawable.drawer_frame);
        view.setEditing(true);
    }

    public void disableEditing() {
        //show a transparent background that user can see behind the view.
        view.setBackgroundResource(android.R.color.transparent);
        view.setEditing(false);
    }

}
