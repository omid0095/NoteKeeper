package com.omid.notekeeper.ui.integration;

import com.omid.notekeeper.ui.IBaseView;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by Omid on 8/31/17.
 */

public interface IntegrationView extends IBaseView {

    void showList(List<File> directories, List<File> noteFiles, boolean showSave);

    void showUpButton(Boolean show);

    void setTitle(boolean isExport);

    Observable<Boolean> askForReplace();

    Observable<String> askFileName();

    Observable<Void> showFileNameEmptyError();

    Observable<Void> showImportDone();

    Observable<Void> showExportDone();
}
