package com.omid.notekeeper.app;

/**
 * Created by Omid on 9/1/17.
 */

public class Constants {

    //Extension of Exporting/Importing backup files
    public static final String NOTE_FILE_EXT = "NKD";
}
