package com.omid.notekeeper.app;

import android.app.Activity;
import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.omid.notekeeper.dagger.component.DaggerDataComponent;
import com.omid.notekeeper.dagger.component.DataComponent;
import com.omid.notekeeper.dagger.module.ActivityModuleProvider;
import com.omid.notekeeper.dagger.module.AppModule;
import com.omid.notekeeper.dagger.module.ContextModule;
import com.omid.notekeeper.dagger.module.FileModule;

/**
 * Created by Omid on 8/30/17.
 */

public class NoteKeeperApplication extends Application {

    private DataComponent component;

    private ActivityModuleProvider activityModuleProvider;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerDataComponent.builder()
                .appModule(new AppModule(this))
                .fileModule(new FileModule())
                .contextModule(new ContextModule())
                .build();
    }

    public DataComponent getComponent() {
        return component;
    }

    public void setComponent(DataComponent component) {
        this.component = component;
    }

    /**
     * this method is usually called by the activity to get its module from the application
     */
    public <T> T provideActivityModule(Activity activity) {
        // Ask for a module for the activity from an ActivityModuleProvider instance.
        return (T) getActivityModuleProvider().getActivityModule(activity);
    }

    private ActivityModuleProvider getActivityModuleProvider() {
        if (activityModuleProvider == null)
            activityModuleProvider = new ActivityModuleProvider();
        return activityModuleProvider;
    }

    @VisibleForTesting
    public void setActivityModuleProvider(ActivityModuleProvider activityModuleProvider) {
        this.activityModuleProvider = activityModuleProvider;
    }
}
