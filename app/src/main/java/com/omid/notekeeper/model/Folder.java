package com.omid.notekeeper.model;

/**
 * Created by Omid on 8/30/17.
 */

public interface Folder {

    /**
     * Each folder has a name
     */
    String getName();

    /**
     * Each folder has some
     * @return number of the folder contents
     */
    int getContentsCount();
}
