package com.omid.notekeeper.model;

import android.graphics.Bitmap;

/**
 * Each Note contains a string and a bitmap value
 * <p>
 * <p>
 * Created by Omid on 8/30/17.
 */

public interface Note {

    /**
     * each Note has a nam
     *
     * @return name of note
     */
    String getName();

    /**
     * each note has a string content.
     *
     * @return string content of the note
     */
    String getString();

    /**
     * user can change string of the note
     */
    void setString(String string);

    /**
     * each note has a bitmap
     *
     * @return the bitmap of the note
     */
    Bitmap getBitmap();

    /**
     * user can change the bitmap of the note
     */
    void setBitmap(Bitmap bitmap);
}
