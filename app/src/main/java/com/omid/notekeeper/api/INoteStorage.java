package com.omid.notekeeper.api;

import android.graphics.Bitmap;

import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.internal.operators.observable.ObservableAll;

/**
 * Note Storage interface provides methods to work with Folders and Notes.
 * These methods are time consuming so the implementation has to run these in
 * background and return an observable.
 * Created by Omid on 8/30/17.
 */

public interface INoteStorage {

    /**
     * this method provides all of the folders in storage
     *
     * @return list of folders
     */
    Observable<List<Folder>> getFolders();

    /**
     * provides all of the notes in a specific folder
     *
     * @param folder
     * @return list of notes
     */
    Observable<List<Note>> getNotes(Folder folder);

    /**
     * this method add a new folder into the storage
     *
     * @param name name of the folder.
     * @return a folder object
     */
    Observable<Folder> addFolder(String name);

    /**
     * add a new note into the specific folder
     *
     * @param folder
     * @param name   name of the note.
     * @return a Note object
     */
    Observable<Note> addNote(Folder folder, String name);

    /**
     * delete a folder
     *
     * @param folder
     * @return an observable to inform user when deleting is done.
     */
    Observable<Void> deleteFolder(Folder folder);

    /**
     * delete a note
     *
     * @param note
     * @return an observable to inform user when deleting is done.
     */
    Observable<Void> deleteNote(Note note);

    /**
     * user can update contents of a Note object, to apply changes into the
     * storage the note should be passed in this method
     *
     * @param note the note which has to be save
     * @return an observable to inform user when saving is done.
     */
    Observable<Void> saveNote(Note note);


}
