package com.omid.notekeeper.api;

/**
 * This interface is for validating input fields
 * Created by Omid on 8/30/17.
 */

public interface InputValidator {

    /**
     * to validate a folder name that is writen by user
     *
     * @param folderName the string name which is going to be checked.
     * @throws Exception in case of being invalid the method throws an exception
     */
    void validateFolderName(String folderName) throws Exception;

    /**
     * to validate a Note name that is writen by user
     *
     * @param name the string name which is going to be checked.
     * @throws Exception in case of being invalid the method throws an exception
     */
    void validateNoteName(String name) throws Exception;
}
