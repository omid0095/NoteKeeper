package com.omid.notekeeper.api;

import com.omid.notekeeper.model.Folder;
import com.omid.notekeeper.model.Note;

/**
 * A context to keep current Note or Folder which user is working with.
 * Created by Omid on 8/31/17.
 */

public class NotesContext {
    private Folder currentFolder;
    private Note currentNote;

    public Folder getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(Folder currentFolder) {
        this.currentFolder = currentFolder;
    }

    public Note getCurrentNote() {
        return currentNote;
    }

    public void setCurrentNote(Note currentNote) {
        this.currentNote = currentNote;
    }
}
