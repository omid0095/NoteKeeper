package com.omid.notekeeper.api;

import android.os.Environment;
import android.webkit.MimeTypeMap;

import com.omid.notekeeper.app.Constants;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.List;

/**
 * This is an assistant in working with files.
 * <p>
 * Created by Omid on 9/1/17.
 */

public class FileManager {

    /**
     * the Root directory to achive files in android can be different places
     * File manager decides where it has to be.
     *
     */
    public File getRootDirectory() {
        // give phone storage directory as the root
        return Environment.getExternalStorageDirectory();
    }

    /**
     * @param dir the parent folder
     * @return directories in the folder as a list of File
     */
    public List<File> getDirectories(File dir) {
        File[] folders = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        return Arrays.asList(folders);
    }

    /**
     *
     * @param dir the parent directory
     * @return the files with (Constants.NOTE_FILE_EXT) extention.
     */
    public List<File> getNoteFiles(File dir) {
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return Constants.NOTE_FILE_EXT.equals(
                        MimeTypeMap.getFileExtensionFromUrl(name));
            }
        });
        return Arrays.asList(files);
    }

    /**
     * copy a file from src to dst
     * @param src path of source file
     * @param dst path of destination file
     * @return if copy is done successfully Or not
     */
    public boolean copy(String src, String dst) {
        try {
            File srcFile = new File(src);
            File dstFile = new File(dst);

            FileChannel srcChannel = new FileInputStream(srcFile).getChannel();
            FileChannel dstChannel = new FileOutputStream(dstFile).getChannel();
            dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
            srcChannel.close();
            dstChannel.close();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }
}
