package com.omid.notekeeper;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerDataComponent;
import com.omid.notekeeper.dagger.component.DataComponent;
import com.omid.notekeeper.dagger.module.ActivityModuleProvider;
import com.omid.notekeeper.dagger.module.AppModule;
import com.omid.notekeeper.dagger.module.ContextModule;
import com.omid.notekeeper.dagger.module.FileModule;
import com.omid.notekeeper.dagger.module.FolderActivityModule;
import com.omid.notekeeper.ui.folder.FolderPresenter;
import com.omid.notekeeper.ui.folder.FolderView;
import com.omid.notekeeper.ui.folder.FoldersActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Instrumentation test, for Folders view which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class FoldersViewTest {

    @Rule
    public ActivityTestRule<FoldersActivity> activityRule =
            new ActivityTestRule<>(FoldersActivity.class, false, false);
    @Mock
    FolderPresenter folderPresenter;
    @Mock
    ActivityModuleProvider moduleProvider;
    @Mock
    FolderActivityModule folderActivityModule;

    @Before
    public void setup() {
        createMockObjects();

        //create dataComponent using mocked objects
        Context appContext = InstrumentationRegistry.getTargetContext();
        DataComponent component = DaggerDataComponent.builder()
                .appModule(new AppModule(appContext))
                .fileModule(mock(FileModule.class))
                .contextModule(new ContextModule())
                .build();

        //set the component to the app
        getApp().setComponent(component);

        //set current moduleProvider to the app
        getApp().setActivityModuleProvider(moduleProvider);

        activityRule.launchActivity(null);

    }

    private void createMockObjects() {
        initMocks(this);
        // return current mock presenter when activityModule is asked for a presenter
        when(folderActivityModule.getFolderPresenter(any(INoteStorage.class), any(NotesContext.class), any(FolderView.class))).thenReturn(folderPresenter);

        //return a mock FolderView when activityModule is asked for.
        when(folderActivityModule.provideFolderView()).thenReturn(mock(FolderView.class));

        //return current mocked folderActivityModule when moduleProvider is asked
        //for an activity module.
        when(moduleProvider.getActivityModule(any(Activity.class))).thenReturn(folderActivityModule);

    }

    private Context getAppContext() {
        return InstrumentationRegistry.getTargetContext();

    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.omid.notekeeper", appContext.getPackageName());
    }

    private NoteKeeperApplication getApp() {
        return (NoteKeeperApplication) InstrumentationRegistry.getInstrumentation()
                .getTargetContext().getApplicationContext();
    }

    @Test
    public void loadFoldersTest() throws InterruptedException {
        /*
            when activity start it is expected that it calls loadFolders once.
         */
        FoldersActivity activity = activityRule.getActivity();
        verify(folderPresenter, times(1)).loadFolders();
    }

    @Test
    public void showMessageTest() throws Throwable {
        /*
        when activity is asked for showMessage, it is expected to show a view with same title
         */

        final FoldersActivity activity = activityRule.getActivity();
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.showMessage("dlg_title", "dlg_text").subscribe();


            }

        });
        Espresso.onView(withText("dlg_title")).check(matches(isDisplayed()));

    }
}
