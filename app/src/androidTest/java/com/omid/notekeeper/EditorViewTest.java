package com.omid.notekeeper;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.omid.notekeeper.api.INoteStorage;
import com.omid.notekeeper.api.NotesContext;
import com.omid.notekeeper.app.NoteKeeperApplication;
import com.omid.notekeeper.dagger.component.DaggerDataComponent;
import com.omid.notekeeper.dagger.component.DataComponent;
import com.omid.notekeeper.dagger.module.ActivityModuleProvider;
import com.omid.notekeeper.dagger.module.AppModule;
import com.omid.notekeeper.dagger.module.ContextModule;
import com.omid.notekeeper.dagger.module.EditorActivityModule;
import com.omid.notekeeper.dagger.module.FileModule;
import com.omid.notekeeper.model.Note;
import com.omid.notekeeper.ui.editor.EditorActivity;
import com.omid.notekeeper.ui.editor.EditorPresenter;
import com.omid.notekeeper.ui.editor.EditorView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Instrumentation EditorActivity test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class EditorViewTest {

    @Rule
    public ActivityTestRule<EditorActivity> activityRule =
            new ActivityTestRule<>(EditorActivity.class, false, false);
    @Mock
    EditorPresenter editorPresenter;
    @Mock
    ActivityModuleProvider moduleProvider;
    @Mock
    EditorActivityModule editorActivityModule;
    @Mock
    ContextModule contextModule;


    /**
     * create mock objects for the EditorActivity
     */
    @Before
    public void setup() {
        createMockObjects();

        //create a data component using mock objects
        DataComponent component = DaggerDataComponent.builder()
                .appModule(new AppModule(getAppContext()))
                .fileModule(mock(FileModule.class))
                .contextModule(contextModule)
                .build();
        //set component to the app
        getApp().setComponent(component);

        //set current mocked moduleProvider to the app
        getApp().setActivityModuleProvider(moduleProvider);

        activityRule.launchActivity(null);

    }

    private void createMockObjects() {
        initMocks(this);
        //return mocked presenter when module is asked for a Presenter
        when(editorActivityModule.getPresenter(any(INoteStorage.class), any(Note.class), any(EditorView.class))).thenReturn(editorPresenter);

        //return a mockView when module is asked for an EditorView
        when(editorActivityModule.provideEditorView()).thenReturn(mock(EditorView.class));

        //return current editorActivityModule when moduleProvider is asked for module
        //for an EditorActivity instance
        when(moduleProvider.getActivityModule(any(EditorActivity.class))).thenReturn(editorActivityModule);

        //return a mock Note when context module is asked for a note
        when(contextModule.getCurrentNote(any(NotesContext.class))).thenReturn(mock(Note.class));

        //return a mock noteContext when context module is asked for a noteContext
        when(contextModule.provideNotesContext()).thenReturn(mock(NotesContext.class));
    }

    private Context getAppContext() {
        return InstrumentationRegistry.getTargetContext();

    }

    private NoteKeeperApplication getApp() {
        return (NoteKeeperApplication) getInstrumentation()
                .getTargetContext().getApplicationContext();
    }

    @Test
    public void bindViewTest() throws InterruptedException {
        /*
        when activity starts it is expected to ask the presenter to bind the view
         */
        EditorActivity activity = activityRule.getActivity();
        verify(editorPresenter, times(1)).bindView();
    }

    @Test
    public void showSaveButton() throws Throwable {

        /*
        when activity is asked for showing save, it is expected to show the button
         */

        final EditorActivity activity = activityRule.getActivity();
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.showSave(true);


            }

        });
        Espresso.onView(withId(R.id.menu_note_save)).check(matches(isDisplayed()));

    }

    @Test
    public void hideSaveButton() throws Throwable {

        /*
        when activity is asked for hiding save, it is expected to hide the button
        */
        final EditorActivity activity = activityRule.getActivity();
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.showSave(false);


            }

        });
        Espresso.onView(withId(R.id.menu_note_save)).check(doesNotExist());

    }

    @Test
    public void showBoldBtnOnOptionCLick() throws Throwable {
        /*
        when Menu button is clicked it is expected that Bold button is shown
         */
        Espresso.onView(withId(R.id.menu_fab)).perform(click());
        Espresso.onView(withId(R.id.bold_fab)).check(matches(isDisplayed()));

    }

    @Test
    public void hideBoldBtnOnRichTextEditorClick() throws Throwable {

        /*
        when Menu button is clicked it is expected that Bold button is shown,
        and on clicking on the rich text, the Bold button has to hide.
         */
        Espresso.onView(withId(R.id.menu_fab)).perform(click());
        Espresso.onView(withId(R.id.bold_fab)).check(matches(isDisplayed()));
        Espresso.onView(withId(R.id.richtext)).perform(click());
        Espresso.onView(withId(R.id.bold_fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));

    }
}
